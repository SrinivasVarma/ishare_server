/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.Utils;

import com.users.Base64;

/**
 *
 * @author srinivas
 */
public class Constant {
    
    public String CODE                  ="CODE";    // CODE FOR DIFF PURPOSE
    public String TOKEN                  ="TOKEN";    // CODE FOR DIFF PURPOSE
    public String MSG                   ="MSG";     // RETURN MESSAGE TO USER
    public String U_MID1                 ="MID1";     // USER IMEI sim1 ID
    public String U_MID2                 ="MID2";     // USER IMEI SIM2 ID
    public String U_FROM_DEVICE         ="DEVICE";  // USER DEVICE ANDROID/IPHONE
    public String U_EMAIL               ="EMAIL";   // USER EMAIL
    public String U_PHONE               ="PHONE";   // USER PHONE
    public String U_NAME                ="NAME";    // USER NAME
    public String U_GPN                 ="GPN";     // GOOGLE PUSH NOTIFICTION   
    public String U_PWD                 ="PWD";     // USER PASSWORD;
    public String U_ID                  ="UID";
       public String U_PIC              ="IMAGE";
    public String U_LOGIN_TYPE                  ="LGTYPE";//0FACEBOOK / 1 GOOGLE
    
    public String FACEBOOK  ="0";
    public String GOOGLE    ="1";
    public int U_TOKEN_ID=11011;
    
    public String USER_ID_INIT="APD_";
    
    
    public String INFO_CODE_SUCCESS        ="05";
    public String INFO_CODE_ALREADY        ="06";//when user is already created.
    public String INFO_CODE_FAILED         ="03";
    
    
     public String USER_SUCCESS_LOGGED          ="USER successfully LOGGED IN";
    public String USER_SUCCESS_CREATED          ="USER successfully created";
    public String USER_SUCCESS_CREATED_ALREADY          ="USER already created";
    public String USER_SUCCESS_VERF_MOBILE          ="USER successfully created, and a verification code has been sent";
    public String USER_SUCCESS_VERF_EMAIL          ="USER successfully created and a verification email has been sent";
     public String USER_SUCCESS_FAILED         ="USER successfully created and but failed to send email or Verification sms";
    public String USER_ALREADY_EXISTS           ="User already created";
    public String USER_DOESNOT_EXISTS           ="EMAIL or Password are incorrect";
    public String USER_CREATION_ERROR           ="Sorry something went wrong, please try again later";
    public String USER_FORGOT_ERROR             ="Sorry something went wrong, please try again later";
    public String USER_FORGOT_SUCCESS           ="A VERIFICATION EMAIL HAS BEEN SENT SUCCESSFULLY TO THE REGISTERED EMAIL, PLEASE CHECK IT ";
    public String USER_FORGOT_FAILED            ="THE GIVE EMAIL DOES NOT EXIST, PLEASE CHECK THE ENTERED EMAIL.";
    
 
    
    public String SetToken(String str){
        return  new String(Base64.encode(str .getBytes(),U_TOKEN_ID) );
    }
    
    public String GetToken(String str){
        return  new String(Base64.decode(str .getBytes(),U_TOKEN_ID) );
    }
    
    public String GetUSERIDfromTOKEN(String str){
        String[] token=str.split(""
                + "\\$");
        
        return  token[0].split(USER_ID_INIT)[1];
    }
    
}
