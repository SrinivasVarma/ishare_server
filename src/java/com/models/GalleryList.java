package com.models;

import org.json.simple.JSONObject;

public class GalleryList {

    String _media_id, _date, _media_path, _media_size, _media_type, _media_tag, _media_cat_name, _media_cat_id, _user_name, _user_image, _user_id, _media_headline, _media_desc;

    String _media_fav,_media_like,_media_views,_media_comment;


//    @Override
    public JSONObject toJsonInfo() {
        JSONObject obj = null;
        try {
            obj = new JSONObject();

            if (_media_headline != null) {
                obj.put("MHEAD", _media_headline);
            }
            if (_media_desc != null) {
                obj.put("MDESC", _media_desc);
            }
            if (_media_id != null) {
                obj.put("MID", _media_id);
            }
            if (_date != null) {
                obj.put("MDATE", _date);
            }
            if (_media_path != null) {
                obj.put("MIMG", _media_path);
            }
            if (_media_size != null) {
                obj.put("MSIZE", _media_size);
            }
            if (_media_type != null) {
                obj.put("MTYPE", _media_type);
            }
            if (_media_tag != null) {
                obj.put("MTAG", _media_tag);
            }
            if (_media_cat_name != null) {
                obj.put("MCTN", _media_cat_name);
            }
            if (_media_cat_id != null) {
                obj.put("MCTID", _media_cat_id);
            }
            if (_user_name != null) {
                obj.put("UNAME", _user_name);
            }
            if (_user_image != null) {
                obj.put("UIMG", _user_image);
            }
            if (_user_id != null) {
                obj.put("UID", _user_id);
            }
            
            if (_media_fav != null) {
                obj.put("MFAV", _media_fav);
            }
            
            if (_media_like != null) {
                obj.put("MLIKE", _media_like);
            }
            
            if (_media_views != null) {
                obj.put("MVIEW", _media_views);
            }
            
            if (_media_comment != null) {
                obj.put("MCOMM", _media_comment);
            }

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
            return null;
        }
        return obj;
    }

    public String getMedia_headline() {
        return _media_headline;
    }

    public void setMedia_headline(String _media_headline) {
        this._media_headline = _media_headline;
    }

    public String getMedia_desc() {
        return _media_desc;
    }

    public void setMedia_desc(String _media_desc) {
        this._media_desc = _media_desc;
    }

    public String getMedia_id() {
        return _media_id;
    }

    public void setMedia_id(String _media_id) {
        this._media_id = _media_id;
    }

    public String getDate() {
        return _date;
    }

    public void setDate(String _date) {
        this._date = _date;
    }

    public String getMedia_path() {
        return _media_path;
    }

    public void setMedia_path(String _media_path) {
        this._media_path = _media_path;
    }

    public String getMedia_size() {
        return _media_size;
    }

    public void setMedia_size(String _media_size) {
        this._media_size = _media_size;
    }

    public String getMedia_type() {
        return _media_type;
    }

    public void setMedia_type(String _media_type) {
        this._media_type = _media_type;
    }

    public String getMedia_tag() {
        return _media_tag;
    }

    public String getMedia_cat_name() {
        return _media_cat_name;
    }

    public void setMedia_cat_name(String _media_cat_name) {
        this._media_cat_name = _media_cat_name;
    }

    public String getMedia_cat_id() {
        return _media_cat_id;
    }

    public void setMedia_cat_id(String _media_cat_id) {
        this._media_cat_id = _media_cat_id;
    }

    public void setMedia_tag(String _media_tag) {
        this._media_tag = _media_tag;
    }

    public String getUser_name() {
        return _user_name;
    }

    public void setUser_name(String _user_name) {
        this._user_name = _user_name;
    }

    public String getUser_image() {
        return _user_image;
    }

    public void setUser_image(String _user_image) {
        this._user_image = _user_image;
    }

    public String getUser_id() {
        return _user_id;
    }

    public void setUser_id(String _user_id) {
        this._user_id = _user_id;
    }
    public String getMedia_fav() {
        return _media_fav;
    }

    public void setMedia_fav(String _media_fav) {
        this._media_fav = _media_fav;
    }

    public String getMedia_like() {
        return _media_like;
    }

    public void setMedia_like(String _media_like) {
        this._media_like = _media_like;
    }

    public String getMedia_views() {
        return _media_views;
    }

    public void setMedia_views(String _media_views) {
        this._media_views = _media_views;
    }

    public String getMedia_comment() {
        return _media_comment;
    }

    public void setMedia_comment(String _media_comment) {
        this._media_comment = _media_comment;
    }
}
