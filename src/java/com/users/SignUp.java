package com.users;

import com.Utils.Constant;
import com.db.DbUtil;
import java.io.IOException;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.json.JSONObject;
import java.sql.Connection;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.json.JSONException;

/**
 * Servlet implementation class SetFollowers
 *
 * `username`, `password`, `email`, `mobile`, `picture`, `status`,
 * `email_verif`, `mobile_verif`, `creationDate`, `modificationDate`, `imei`,
 * `imsi`)
 */
@WebServlet(value = "/register", name = "/SignUp")
public class SignUp extends HttpServlet {

//    SendSMS sendSMS=null;
    private static final long serialVersionUID = 1L;
    private String INPUT = "";
    public Constant constant = new Constant();

    /**
     * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
     * response)
     */
    @SuppressWarnings("unchecked")
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        INPUT = request.getParameter("id");
        JSONObject res = new JSONObject();

        System.err.println("Received Data : " + INPUT);
        try {
            if (INPUT != null && !INPUT.equals("")) {
                JSONObject Obj = new JSONObject(INPUT);
                res = StorenewUser(Obj);

            } else {
                res.put(constant.CODE, constant.INFO_CODE_FAILED);
                res.put(constant.MSG, "Sorry , Required data missing");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.err.println("Response From Server :\n Data : " + res.toString());
        response.getWriter().write(res.toString());
    }

    public void outprint(String message) {

        System.out.println("" + message);

    }

    //{"GPN":"APA91bFND2Oi-vsLTt3nbWUZJRPRmOqZ6vuzPhaZKVBbsyHIrRPZT9nzBkcxQYos3S2ZbLwJVtkyTUiR_J18-i3-du1tHsvmtIBnUNWqfnpEXlZBE3f3sYlRyP2XvBXnMn7yZBgVTGvP",
//"NAME":"Srinivas Varma",
//"IMAGE":"https:\/\/graph.facebook.com\/1419534298070865\/picture?type=large",
//"EMAIL":"prabas.2u@gmail.com","EMAILLIST":{"com.google":"srinivas.p@way2online.net","com.facebook.messenger":"Messenger","com.microsoft.office":"Office","com.facebook.auth.login":"prabas.2u@gmail.com","bsbportal.com":"dummyaccount","com.whatsapp":"WhatsApp"},
//        "PHONE":"1234567890",
//"DEVICE":"null",
//"MID2":"faf78ac119e60caa",
//"MID1":"863664034443332"}
    public JSONObject StorenewUser(JSONObject data) {
        JSONObject resp = new JSONObject();
        try {
            try {
                PreparedStatement st;
                resp = hasRecord(data);

                if (resp != null && !resp.has("TOKEN")) {
                    //NEW FOLLOWER INSERTING
                    outprint("INSERTING");

                    String EMAIL = "",
                            MID1 = "",
                            MID2 = "",
                            PHONE = "",
                            GPN = "",
                            FROMDEV = "",
                            UDD = "",
                            NAME = "",
                            TOKEN = "",
                            LG_PROFTYPE = "", //0- FACEBOOK,1-GOOGLE
                            LG_PROF_IMG = "";

                    Connection conn = null;
                    PreparedStatement checkID = null;
                    PreparedStatement stmtreg = null;
                    PreparedStatement stmtvef = null;

                    try {
                        MID1 = (String) (data.has(constant.U_MID1) ? data.getString(constant.U_MID1) : null);
                        MID2 = (String) (data.has(constant.U_MID2) ? data.getString(constant.U_MID2) : null);
                        NAME = data.getString(constant.U_NAME);
                        EMAIL = data.getString(constant.U_EMAIL);
                        GPN = data.getString(constant.U_GPN);
                        LG_PROF_IMG = data.getString(constant.U_PIC);
                        
                        LG_PROFTYPE = data.getString(constant.U_LOGIN_TYPE);
                        FROMDEV = data.getString(constant.U_FROM_DEVICE);
                        PHONE = data.getString(constant.U_PHONE);
                    } catch (Exception ex) {
                        ex.printStackTrace();
//                    Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE, null, ex);
                    }

                    try {
                        if (conn == null) {
                            conn = DbUtil.getConnection();

                            String sqlreg = "INSERT INTO ad_user_data( _user_name,_user_email,_user_phone,_user_token,_user_imei1,_user_imei2,_user_gcm,active,_user_from_login, _user_image_url) "
                                    + "VALUES (?,?,?,?,?,?,?,?,?,?)";
                            stmtreg = conn.prepareStatement(sqlreg, Statement.RETURN_GENERATED_KEYS);
                            stmtreg.setString(1, NAME);
                            stmtreg.setString(2, EMAIL);
                            stmtreg.setString(3, PHONE);
                            stmtreg.setString(4, TOKEN);
                            stmtreg.setString(5, MID1);
                            stmtreg.setString(6, MID2);
                            stmtreg.setString(7, GPN);
                            stmtreg.setString(8, "1");
                            stmtreg.setString(9, LG_PROFTYPE);
                            stmtreg.setString(10, LG_PROF_IMG);
                            int i = 0;
                            if (stmtreg.executeUpdate() != 0) {
                                ResultSet rs = stmtreg.getGeneratedKeys();
                                rs.next();
                                i = rs.getInt(1);
                                TOKEN = constant.SetToken(constant.USER_ID_INIT + i + "$" + PHONE + "$" + MID1);
                            }

                            resp.put(constant.CODE, constant.INFO_CODE_SUCCESS);
                            resp.put(constant.TOKEN, TOKEN);
                            resp.put(constant.MSG, constant.USER_SUCCESS_CREATED);

                        }
                    } catch (SQLException ex) {
                        ex.printStackTrace();
//                    Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE, null, ex);
                    } finally {
                        if (stmtreg != null) {
                            stmtreg.close();
                        }
                    }
//        return "failed";
                } else {

                    resp.put(constant.CODE, constant.INFO_CODE_ALREADY);

                }
            } catch (SQLException ex) {
                ex.printStackTrace();
//            Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
//                                    Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE, null, ex);
        }

        return resp;
    }

    private JSONObject hasRecord(JSONObject data) throws SQLException {
        JSONObject resObj = new JSONObject();
        String ProfileImage = "";
        String UserEmail = "";
        String LOGINTYPE = "";

        PreparedStatement UptStmt = null, NewUptstmt = null;
        String sql = "Select _id,_user_email,_user_imei1,_user_image_url from ad_user_data where _user_email = ? AND  _user_from_login=? AND active='1'  LIMIT 1";
        try {
outprint("CHECK 0 "+sql);
            UserEmail = data.getString(constant.U_EMAIL);
            LOGINTYPE = data.getString(constant.U_LOGIN_TYPE);
            try {
                Connection conn = DbUtil.getConnection();
                PreparedStatement ps = conn.prepareStatement(sql);

                ps.setString(1, UserEmail);
                ps.setString(2, LOGINTYPE);
                ResultSet rs = ps.executeQuery();

                if (rs.next()) {
                      outprint("CHECK 1");
                    ProfileImage = data.getString(constant.U_PIC);
                    if (ProfileImage != null && !ProfileImage.equals("")) {

                        String update = "UPDATE ad_user_data SET _user_image_url=? WHERE _user_email=?";

                        UptStmt = conn.prepareStatement(update);
                        UptStmt.setString(1, ProfileImage);
                        UptStmt.setString(2, UserEmail);

                        int rowsUpdated = UptStmt.executeUpdate();
                        if (rowsUpdated > 0) {
                            resObj.put("INFO", "Profile Pic is Updated");
                        }
                         outprint("CHECK 1 "+update);
                    }
                    resObj.put(constant.TOKEN, constant.SetToken(constant.USER_ID_INIT + rs.getString(rs.findColumn("_id"))
                            + "$" + rs.getString(rs.findColumn("_user_email"))
                            + "$" + rs.getString(rs.findColumn("_user_imei1"))));//rs.getString(rs.findColumn("_user_token"));
                } else {
                     outprint("CHECK 2");
                    String update = "UPDATE ad_user_data SET active=? WHERE _user_email=? AND _user_from_login=? ";
                    NewUptstmt = conn.prepareStatement(update);
                    NewUptstmt.setString(1, "0");
                    NewUptstmt.setString(2, UserEmail);
                    NewUptstmt.setString(3, LOGINTYPE.equals("0") ? "1" : "0");
                    int rowsUpdated = NewUptstmt.executeUpdate();
                    if (rowsUpdated > 0) {
                        resObj.put("INFO", "New Profile Will be created");
                    }
                    outprint("CHECK 2 "+update);
                }
                return resObj;
            } catch (SQLException ex) {
                ex.printStackTrace();
//                    Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE, null, ex);
            } finally {
                if (UptStmt != null) {
                    UptStmt.close();
                }
                if (NewUptstmt != null) {
                    NewUptstmt.close();
                }
            }
        } catch (JSONException ex) {
            try {
                ex.printStackTrace();
                resObj.put(constant.CODE, constant.INFO_CODE_SUCCESS);
                resObj.put(constant.MSG, ex.getMessage());
            } catch (JSONException ex1) {
                Logger.getLogger(SignUp.class.getName()).log(Level.SEVERE, null, ex1);
            }
        }
        return new JSONObject();
    }

    private JSONObject UpdateUserdata(JSONObject data) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

}
