/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.getMedia;

import com.Utils.Constant;
import com.db.DbUtil;
import com.models.GalleryList;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author srinivas
 */
@WebServlet(value = "/posthome", name = "/getMediaList")
public class getMediaList extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private String TOKEN = "";
    private String Start = "";
    private String End = "";

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println(" Server :: " + request.getParameter("id"));
        JSONObject obj = null;
        try {
            JSONParser parser = new JSONParser();
            obj = new JSONObject(request.getParameter("id"));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        response.getWriter().append(getDatafromStore(obj).toString());

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public Constant constant = new Constant();

    private JSONObject getDatafromStore(JSONObject obj) {
        // TODO Auto-generated method stub
         System.out.println(" Server :: Step 1" );
        JSONObject res = new JSONObject();
        try {
            TOKEN =  obj.getString("TOKEN");
            Start =  obj.getString("START");
            
            System.out.println(" Server :: Step 2 "+TOKEN+"   :  "+Start );

            PreparedStatement stmt = null;
            String sql = "SELECT "
                    + "t2._user_name,"
                    + "t2._user_image_url,"
                    + "t3._cat_name ,"
                    + "t1._media_category_id,"
                   
                    + "t1._media_path,"
                    + "t1._media_size,"
                    + "t1._media_type,"
                    + "t1._media_head,"
                    + "t1._media_desc,"
                    + "t1._media_tag,"
                     + "t1.creationdate,"
                    + "t1._media_views_count,"
                    + "t1._media_like_count,"
                    + "t1._media_fav_count,"
                    + "t1._media_comment_count,"
                     + "t1._id "
                    + "FROM ad_media as t1 "
                    + "inner join ad_user_data as t2  ON t1._user_id=t2._id "
                    + "inner join media_category as t3 on t1._media_category_id=t3._id  "
                    + "where t2.active='1' and t1.active='1' and t1._media_mode!='2' order by t1.creationdate desc limit " + Start + ",5";

            System.out.println(" Server :: Step 3 "+sql );
            try {
                stmt = DbUtil.getConnection().prepareStatement(sql);

                JSONArray  arrObj = new JSONArray();
                ResultSet result = stmt.executeQuery();
                while (result.next()) {
                   
                    GalleryList prod = new GalleryList();
                    prod.setUser_name(result.getString(1));
                    prod.setUser_image(result.getString(2));
                    prod.setMedia_cat_name(result.getString(3));
                    prod.setMedia_cat_id(result.getString(4));
                    prod.setMedia_path(result.getString(5));
                    prod.setMedia_size(result.getString(6));

                    prod.setMedia_type(result.getString(7));
                    prod.setMedia_headline(result.getString(8));
                    prod.setMedia_desc(result.getString(9));
                    prod.setMedia_tag(result.getString(10));
                    prod.setDate(result.getString(11));
                     prod.setMedia_views(result.getString(12));
                      prod.setMedia_like(result.getString(13));
                       prod.setMedia_fav(result.getString(14));
                        prod.setMedia_comment(result.getString(15));
                    prod.setMedia_id(result.getString(16));
                    arrObj.put(prod.toJsonInfo());

                }
                res.put(constant.CODE, constant.INFO_CODE_SUCCESS);
                res.put("data", arrObj);
            } catch (SQLException ex) {
                res.put(constant.CODE, constant.INFO_CODE_FAILED);
                res.put(constant.MSG, "" + ex.getMessage());
            } finally {
                if (stmt != null) {
                    try {
                        stmt.close();
                    } catch (SQLException ex) {
                    }
                }
            }

        } catch (JSONException e) {
            System.out.println(" Server :: Step 4 "+e.getMessage() );
            return null;
        }
        return res;
    }

    public void outprint(String message) {

        System.out.println("" + message);

    }
}
