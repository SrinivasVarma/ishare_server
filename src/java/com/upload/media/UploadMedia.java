package com.upload.media;

import com.Utils.Constant;
import com.db.DbUtil;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringBufferInputStream;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;


import sun.rmi.runtime.Log;

/**
 * Servlet implementation class UploadFiles
 */
@WebServlet(value = "/upload", name = "/UploadMedia")
public class UploadMedia extends HttpServlet {
	private static final long serialVersionUID = 1L;

//	 private final String UPLOAD_DIRECTORY ="/var/lib/tomcat7/webapps/WYoo/Uploads";
//	 private String DOWNLOAD_DIRECTORY = "http://whatsyoo.com/WYoo/Uploads/";

	private final String UPLOAD_DIRECTORY = "/var/lib/tomcat7/webapps/AppGuru/Uploads";
	private String DOWNLOAD_DIRECTORY = "http://domain:8080/AppGuru/Uploads/";
        
        String relativeWebPath = "/WEB-INF/Uploads";
        

	private final int THRESHOLD_SIZE = 1024 * 1024 * 3; // 3MB
	private final int MAX_FILE_SIZE = 1024 * 1024 * 40; // 40MB
	private final int MAX_REQUEST_SIZE = 1024 * 1024 * 50; // 50MB

	
	private String MEDIA_TYPE,MEDIA_OLD_NAME,MEDIA_NEW_NAME,MEDIA_SIZE="";

	// public String UserID,UserJID;

	public String NODATA = "Request does not contain upload data";

	private String result = "";
	boolean isUser = false;
	private InputStream inputStream;
        public Constant  constant = new Constant();

	JSONObject obj = new JSONObject();
        
        String USERID="",USER_MID="",IMG_CAT="",MEDIA_MODE="",USER_MEDIA_TYPE="";
        String MEDIA_HEADLINE,MEDIA_DESC,MEDIA_TAG="";

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		// TODO Auto-generated method stub

	
		MEDIA_TYPE="";
		MEDIA_OLD_NAME="";
		MEDIA_NEW_NAME="";
		MEDIA_SIZE="";
                boolean isMultipart = ServletFileUpload.isMultipartContent(request);

                                
		try {
			// checks if the request actually contains upload file
			if (!ServletFileUpload.isMultipartContent(request)) {
				obj.put("CODE", constant.INFO_CODE_FAILED);
				obj.put("INFO", NODATA);
				return;
			}else{

			// configures upload settings
			DiskFileItemFactory factory = new DiskFileItemFactory();
			factory.setSizeThreshold(THRESHOLD_SIZE);
			factory.setRepository(new File(System.getProperty("java.io.tmpdir")));

			ServletFileUpload upload = new ServletFileUpload(factory);
			upload.setFileSizeMax(MAX_FILE_SIZE);
			upload.setSizeMax(MAX_REQUEST_SIZE);

			// constructs the directory path to store upload file
			// String uploadPath = getServletContext().getRealPath("")
			// + File.separator + UPLOAD_DIRECTORY;
			String uploadPath =UPLOAD_DIRECTORY;
			System.err.println("File Path 1: " + uploadPath);
			// creates the directory if it does not exist
			

			try {
				// parses the request's content to extract file data
				List formItems = upload.parseRequest(request);
				Iterator iter = formItems.iterator();
System.out.println("Stored path : 1" );
				while (iter.hasNext()) {
                                    System.out.println("Stored path : 2" );
					FileItem item = (FileItem) iter.next();
					// processes only fields that are not form fields
					if (!item.isFormField() && isUser) {
						// System.out.println(" USERNAME :: "+item.toString());
                                                
                                                String fileName = FilenameUtils.getName(item.getName());
						
						MEDIA_TYPE=fileName.split("\\.(?=[^\\.]+$)")[1];
						MEDIA_OLD_NAME=fileName;
						MEDIA_SIZE=convertToSIZE(item.getSize())+"";
						System.out.println("Original File Name : "+fileName +"  File Type : "+item.getContentType()+" File Size : "+item.getSize());
						
						fileName=fileName.replace(fileName, USERID+"_"+System.currentTimeMillis())+"."+MEDIA_TYPE;
						
						System.out.println("Renamed File Name : "+fileName+" File Size : "+MEDIA_SIZE);
						MEDIA_NEW_NAME=fileName;
						String filePath = uploadPath + File.separator +getMimeType(fileName,0);
						File uploadDir = new File(filePath);
						if (!uploadDir.exists()) {
							uploadDir.mkdirs();
						}
						System.err.println("Final File Path 1: " + filePath);
						
						File storeFile = new File(filePath+File.separator+ fileName);

						item.write(storeFile);
//                                                
						File FinalImageURL = new File(  USERID + File.separator +getMimeType(fileName,0)+File.separator+ MEDIA_NEW_NAME);
//						
						obj.put("CODE", constant.INFO_CODE_SUCCESS);
						obj.put("INFO", "Successfully Uploaded");
						obj.put("FileSize", MEDIA_SIZE);
						obj.put("FileName", MEDIA_NEW_NAME);
						obj.put("FilePath",FinalImageURL.getPath());
//                                 
				StoreInDatabase(USERID, 
                                                USER_MID, 
                                                IMG_CAT, 
                                               DOWNLOAD_DIRECTORY.replace("domain", "/"+request.getServerName()) + FinalImageURL.getPath(), 
                                                MEDIA_SIZE,
                                                getMimeType(fileName,1),
                                                MEDIA_MODE,
                                                MEDIA_HEADLINE,
                                                MEDIA_DESC,
                                                MEDIA_TAG);
                                                                                      System.out.println("Stored path : 3-5 :"+FinalImageURL.getPath());
					} else {
                                            System.out.println("Stored path : 4" );
						if (item.getFieldName().equals("id")) {
//							System.out.println(" USERNAME :: " + item.getString());

							JSONParser parser = new JSONParser();
							JSONObject Obj = (JSONObject) parser.parse(item.getString());
							if (Obj.containsKey("TOKEN")) {
                                                            String[] tok=constant.GetToken((String) Obj.get("TOKEN")).split("\\$");
//								FROM_JID = (String) Obj.get("FROM_JID");
                                                                USERID=tok[0];
							}
                                                        if(Obj.containsKey("SRTYPE")){
                                                            USER_MEDIA_TYPE= (String) Obj.get("SRTYPE");
                                                        }
                                                        if (Obj.containsKey("IMGCAT")) {
                                                        	IMG_CAT = (String) Obj.get("IMGCAT");
                                                        }
                                                        if (Obj.containsKey("MODE")) {
								MEDIA_MODE = (String) Obj.get("MODE");
                                                        }
							if (Obj.containsKey("TITLE")) {
								MEDIA_HEADLINE = (String) Obj.get("TITLE");
                                                        }
							if (Obj.containsKey("DESC")) {
								MEDIA_DESC = (String) Obj.get("DESC");
                                                        }
							if (Obj.containsKey("TAG")) {
								MEDIA_TAG = (String) Obj.get("TAG");
                                                        }
							
							uploadPath = uploadPath + File.separator + USERID;
							// JSONObject res=new
							// JSONObject(item.getString().toString());
							
							isUser = true;
                                                


						}
					}
				}
			} catch (Exception ex) {
				// result = ("message There was an error: " + ex.getMessage());
				obj.clear();
				obj.put("CODE", constant.INFO_CODE_FAILED);
				obj.put("INFO", ex.getMessage());
			}
			result = obj.toString();
			inputStream = new StringBufferInputStream(result);
			response.setContentType("application/json");
			response.setCharacterEncoding("utf-8");
			PrintWriter out = response.getWriter();
			out.print(result);
			out.flush();
                        }
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
        
//        INSERT INTO `app_db`.`ad_images`
//(`_id`,
//`_user_id`,
//`_user_imei`,
//`_img_category_id`,
//`_img_path`,
//`_img_size`,
//`creationdate`,
//`active`)

	private String StoreInDatabase(String USER_ID, String USER_MID, String IMG_CAT, String IMG_PATH, String IMG_SIZE,String MEDIA_TYPE,String MEDIA_MODE,String MEDIA_HEADLINE,String MEDIA_DESC,String MEDIA_TAG) {
		// TODO Auto-generated method stub

		PreparedStatement st;
		try {

                    System.out.println("USER ID : "+USER_ID);
			String sql = "insert into ad_media "
					+ " (_user_id,_user_imei,_media_category_id,_media_path,_media_size,_media_type,_media_mode,_media_head,_media_desc,_media_tag) "
					+ "VALUES (?,?,?,?,?,?,?,?,?,?)";
                         st = DbUtil.getConnection().prepareStatement(sql);
                          st.setString(1, USER_ID.split(constant.USER_ID_INIT)[1]);
                            st.setString(2, USER_MID);
                            st.setString(3, IMG_CAT);
                            st.setString(4, IMG_PATH);
                            st.setString(5, IMG_SIZE);
                            st.setString(6, MEDIA_TYPE);
//                            st.setString(7, "1");
                            st.setString(7, MEDIA_MODE);
                            st.setString(8, MEDIA_HEADLINE);
                            st.setString(9, MEDIA_DESC);
                            st.setString(10, MEDIA_TAG);
			st.executeUpdate();

			result = "Success";

		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			result = e.getMessage();
		}
		return result;
	}
	
	  private  final long K = 1024;
	    private final long M = K * K;
	    private final long G = M * K;
	    private final long T = G * K;

	    public  String convertToSIZE(final long value){
	        final long[] dividers = new long[] { T, G, M, K, 1 };
	        final String[] units = new String[] { "TB", "GB", "MB", "KB", "B" };
	        if(value < 1)
	            throw new IllegalArgumentException("Invalid file size: " + value);
	        String result = null;
	        for(int i = 0; i < dividers.length; i++){
	            final long divider = dividers[i];
	            if(value >= divider){
	                result = format(value, divider, units[i]);
	                break;
	            }
	        }
	        return result;
	    }
	    
	    private  String format(final long value,
                final long divider,
                final String unit){
final double result =
divider > 1 ? (double) value / (double) divider : (double) value;
return new DecimalFormat("#,##0.#").format(result) + " " + unit;
}

	private final String IMAGE_PATTERN = "([^\\s]+(\\.(?i)(jpg|png|gif|bmp))$)";
	private final String VIDEO_PATTERN = "([^\\s]+(\\.(?i)(mp4|wmv|mpeg|avi|3gp))$)";
	private final String AUDIO_PATTERN = "([^\\s]+(\\.(?i)(mp3|wav))$)";
	private final String FILE_PATTERN = "([^\\s]+(\\.(?i)(doc|docs|pdf|xls|xlxs))$)";

	public  String getMimeType(String fileUrl,int info) throws java.io.IOException {
		Pattern pattern;
		Matcher matcher;

		pattern = Pattern.compile(IMAGE_PATTERN);
		matcher = pattern.matcher(fileUrl);
		if (matcher.matches()) {
			return info==0?"IMAGE":"1";
		}

		pattern = Pattern.compile(VIDEO_PATTERN);
		matcher = pattern.matcher(fileUrl);
		if (matcher.matches()) {
			return info==0?"VIDEO":"3";
		}

		pattern = Pattern.compile(AUDIO_PATTERN);
		matcher = pattern.matcher(fileUrl);
		if (matcher.matches()) {
			return info==0?"AUDIO":"2";
		}

		pattern = Pattern.compile(FILE_PATTERN);
		matcher = pattern.matcher(fileUrl);
		if (matcher.matches()) {
			return info==0?"FILE":"4";
		}

		return "OTHERS";
	}

}
