/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.counts;

import com.getMedia.*;
import com.Utils.Constant;
import com.db.DbUtil;
import com.models.GalleryList;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.simple.parser.JSONParser;

/**
 *
 * @author srinivas
 */
@WebServlet(value = "/mediainfo", name = "/setInfo")
public class setInfo extends HttpServlet {

    private static final long serialVersionUID = 1L;
    private String TOKEN = "";
    private String TYPE = "",CHECK="",ORGTYPE="",MEDTYPE="";
private String MEDIAID = "",IPADDRESS="";
    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        System.out.println(" Server :: " + request.getParameter("id"));
        JSONObject obj = null;
        try {
            IPADDRESS = request.getHeader("X-FORWARDED-FOR");  
   if (IPADDRESS == null) {  
       IPADDRESS = request.getRemoteAddr();  
   } 
            JSONParser parser = new JSONParser();
            obj = new JSONObject(request.getParameter("id"));

        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }


        response.getWriter().append(getDatafromStore(obj).toString());

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    public Constant constant = new Constant();

    private JSONObject getDatafromStore(JSONObject obj) {
        // TODO Auto-generated method stub
        JSONObject res = new JSONObject();
        try {
            TOKEN =  obj.getString("TOKEN");
            TYPE =  obj.getString("TYPE");
            CHECK =  obj.getString("CHECK");
           ORGTYPE= TYPE ;
            MEDIAID =  obj.getString("MEDIAID");
            
            
            if(TYPE.equals("0")){
                TYPE="_views";
                MEDTYPE="_media_views_count";
            }else if(TYPE.equals("1")){
                TYPE="_like";
                 MEDTYPE="_media_like_count";
            }else if(TYPE.equals("2")){
                TYPE="_fav";
                 MEDTYPE="_media_fav_count";
            }else if(TYPE.equals("3")){
                TYPE="_comment";
                 MEDTYPE="_media_comment_count";
            }
            PreparedStatement stmtCheck=null,smtCount = null,smtMedia=null;
            Connection mConn=null;
            String CHECKING = "INSERT INTO ad_media_info "
                    + "(_user_id,_media_id,"+TYPE+",_ip_address) "
                    + "VALUES ('"+constant.GetUSERIDfromTOKEN(constant.GetToken(TOKEN))+"','"+MEDIAID+"',1,'"+IPADDRESS+"')" +
                    "  ON DUPLICATE KEY UPDATE "+TYPE+"='"+CHECK+"';";

            String COUNTING = "select Count(*) from ad_media_info where _media_id='"+MEDIAID+"' and "+TYPE+"='1' and active='1';";
            
            String SETCOUNT="";
            if(CHECK.equals("0")){
             SETCOUNT=    "UPDATE ad_media " +
                                "  SET "+MEDTYPE+" = IF("+MEDTYPE+" > 0, "+MEDTYPE+" - 1, 0)" +
                                "  WHERE _id = '"+MEDIAID+"' ";
            }else{
                SETCOUNT=    "UPDATE ad_media " +
                                "  SET "+MEDTYPE+" = IF("+MEDTYPE+" >= 0, "+MEDTYPE+" + 1, 0)" +
                                "  WHERE _id = '"+MEDIAID+"' ";
            }

            try {
                mConn=DbUtil.getConnection();
                stmtCheck = mConn.prepareStatement(CHECKING);
                smtMedia=mConn.prepareStatement(SETCOUNT);
                JSONArray  arrObj = new JSONArray();
                int result = stmtCheck.executeUpdate();
                if (result!=0) {
                     smtCount = mConn.prepareStatement(COUNTING);
                     ResultSet rs=smtCount.executeQuery();
                     if(rs.next()){
                            res.put(constant.CODE, constant.INFO_CODE_SUCCESS);
                            res.put("COUNT", rs.getInt(1));
                            res.put("MEDIAID",MEDIAID);
                            res.put("TYPE", ORGTYPE);
                            res.put("MEDIAQT", SETCOUNT);
                            res.put("MEDIAUPT", smtMedia.executeUpdate());
                            
                     }
                     

                }
                
            } catch (SQLException ex) {
                res.put(constant.CODE, constant.INFO_CODE_FAILED);
                res.put(constant.MSG, "" + ex.getMessage());
            } finally {
                if (stmtCheck != null) {
                    try {
                        stmtCheck.close();
                    } catch (SQLException ex) {
                    }
                }
                if (smtCount != null) {
                    try {
                        smtCount.close();
                    } catch (SQLException ex) {
                    }
                }
                if (smtMedia != null) {
                    try {
                        smtMedia.close();
                    } catch (SQLException ex) {
                    }
                }
            }

        } catch (JSONException e) {
            return null;
        }
        return res;
    }

    public void outprint(String message) {

        System.out.println("" + message);

    }
}
